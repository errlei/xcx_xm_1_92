import './utils/http' // 一定要执行自己定义的js文件，不然里面的代码不生效
import './utils/utils' // 这个import 不需要导入变量，只需要执行那个js
// import utils from '../../utils/utils'
// 在其他页面里面 通过 getApp()
// app.js
App({
  // token 在全局存放的位置可以是直接存在第一层，或者通过一个对象globalData保存起来
  // 下面两种方式都可以
  token: '',
  refreshToken: '',
  // globalData: {
  //   // utils
  //   token: 'xxxxxxxx'
  // },
  onLaunch() {
    this.getToken()

    // // 获取系统信息
    // const systemInfo = wx.getSystemInfoSync();
    // // 胶囊按钮位置信息
    // const menuButtonInfo = wx.getMenuButtonBoundingClientRect();
    // console.log(333, systemInfo, menuButtonInfo);
    // // 导航栏高度 = 状态栏到胶囊的间距（胶囊上坐标位置-状态栏高度） * 2 + 胶囊高度 + 状态栏高度
    // this.globalData.navBarHeight = (menuButtonInfo.top - systemInfo.statusBarHeight) * 2 + menuButtonInfo.height + systemInfo.statusBarHeight;
    // // 状态栏和菜单按钮(标题栏)之间的间距
    // // 等同于菜单按钮(标题栏)到正文之间的间距（胶囊上坐标位置-状态栏高度）
    // this.globalData.menuBottom = menuButtonInfo.top - systemInfo.statusBarHeight;
    // // 菜单按钮栏(标题栏)的高度
    // this.globalData.menuHeight = menuButtonInfo.height;
    // this.globalData.top = menuButtonInfo.top;
    // this.globalData.leftWidth = systemInfo.screenWidth - menuButtonInfo.width - (systemInfo.screenWidth - menuButtonInfo.right) * 2;
  },
  // 数据都是根据当前机型进行计算，这样的方式兼容大部分机器
  globalData: {
    // top: 0,
    // leftWidth: 0,
    // //全局数据管理
    // navBarHeight: 0, // 导航栏高度
    // menuBottom: 0, // 胶囊距底部间距（顶部间距也是这个）
    // menuHeight: 0, // 胶囊高度
  },
  // 设置从本地储存获取token的方法
  getToken() {
    const that = this
    wx.getStorage({
      key: 'hm_token',
      success(res) {
        // 凡是涉及到this的情况，一定要看this使用的位置，如果有多个函数
        // 就要看this的指向问题。 因为this是函数调用的时候，动态产生的一个变量，
        // 一般可以通过修改箭头函数，或者外部定义变量保存外部的this 这两种方法来保证this指向
        that.token = res.data
      }
    })
    wx.getStorage({
      key: 'hm_refreshToken',
      success(res) {
        that.refreshToken = res.data
      }
    })
  },
  setToken(token, refreshToken) {
    // 获取到token以后，一般要存两个地方： 1个地方是 内存上的变量 token， 1个地方是持久化的本地储存
    this.token = 'Bearer ' + token
    this.refreshToken = 'Bearer ' + refreshToken

    wx.setStorage({
      key: 'hm_token',
      data: 'Bearer ' + token
    })
    wx.setStorage({
      key: 'hm_refreshToken',
      data: 'Bearer ' + refreshToken
    })
  }
})