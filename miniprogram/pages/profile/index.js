Page({
  data: {
    nickName: '',
    avatar: ''
  },

  // 函数的参数 可以直接解构，在我们确定知道别人的对象内容的时候
  onLoad({
    nickName,
    avatar
  }) {
    console.log(111, nickName, avatar);
    this.setData({
      nickName,
      avatar
    })
  },

  getChooseavatar(e) {
    // 本地将上传的数据保存以后，等下还需要将这个 e.detail.avatarUrl 发请求，保存在服务器里面，不然一刷新数据就没有
    // 下面的代码不是我们自己封装的 wx.http方法，所以一些 基地址，拦截器里面的内容 在这里要在写一遍
    wx.uploadFile({
      url: 'https://live-api.itheima.net/upload', //仅为示例，非真实的接口地址
      filePath: e.detail.avatarUrl,
      header: {
        Authorization: getApp().token
      },
      name: 'file',
      formData: {
        type: 'avatar',
      },
      // file: e.detail.avatarUrl,
      success: (res) => {
        // const data = res.data
        const data = JSON.parse(res.data)

        if (data.code !== 10000) return wx.utils.toast('上传失败')
        // 1. this指向问题的bug， 2.  xxx of undefined的报错  3. 做一个错误处理
        this.setData({
          avatar: data.data?.url
        })
      }
    })
  },

  async updateNickname(e) {
    if (e.detail.value == '') return

    this.setData({
      nickName: e.detail.value
    })

    // 下面在发请求
    const res = await wx.http.put('/userInfo', {
      // nickName: this.data.nickName
      nickName: e.detail.value
    })

    // 请求失败
    if (res.code !== 10000) return wx.utils.toast('更改用户昵称失败')


  }


})