Page({

  data: {
    // 胶囊按钮的 高度
    menuHeight: 0,
    // 胶囊按钮距离顶部的距离 （有没有刘海都可以，自动计算）
    menuTop: 0,
    //  胶囊按钮左右的间隔
    menuRight: 0,
    // 我们自定义导航 里面输入框的 外部盒子 宽度
    customViewWidth: 0,

    customeNavHeight: 0
  },

  onLoad(options) {
    // 我之前在公司里面，做过一个小程序，产品经理有个需求，是要我们写自定义的导航栏，我通过查阅资料，结合连个微信内部的api方法 getMenuButtonBoundingClientRect  getSystemInfoSync。 主要是获取 当前胶囊的宽高和距离左右上下的距离， 再就是获取当前设备的一些信息。----  自己封装过自定义导航栏的组件，所有的数据放在全局app.js里面， 通过getApp去访问
    const menu = wx.getMenuButtonBoundingClientRect()

    console.log(111, menu);

    const systemInfo = wx.getSystemInfoSync()
    console.log(222, systemInfo);

    // systemInfo.statusBarHeight 状态栏的高度  iphone6  20
    const tmpHeight = systemInfo.statusBarHeight + (menu.top - systemInfo.statusBarHeight) * 2 + menu.height
    const customViewWidthTmp = systemInfo.screenWidth - (systemInfo.screenWidth - menu.right) * 2 - menu.width

    this.setData({
      menuHeight: menu.height,
      menuTop: menu.top,
      menuRight: systemInfo.screenWidth - menu.right,
      customViewWidth: customViewWidthTmp,
      customeNavHeight: tmpHeight
    })
  },

})