let myCode = ''
const app = getApp()

Page({
  data: {
    mobile: '',
    code: '',
    countDownVisible: false,
  },
  onLoad(option) {
    console.log('进入了登录页面:', option);
    // wx.getClipboardData()
    this.setData({
      url: option.aaaa
    })
  },

  // 发起登录请求
  async submitForm() {
    // 函数里面先写所有失败的情况，最后在处理正常的逻辑
    const valideMobileRes = this.validateMobile(this.data.mobile)
    const validateCodeRes = this.validateCode()

    if (!valideMobileRes || !validateCodeRes) return

    // 下面代码才是所有检验通过以后，执行的地方，比如等下发请求等等
    const res = await wx.http.post('/login', {
      mobile: this.data.mobile,
      code: this.data.code
    })

    if (res.code !== 10000) return wx.utils.toast(res?.message || '登录请求失败')

    app.setToken(res.data.token, res.data.refreshToken)

    // 登录请求成功以后，回到之前的页面，如果没有，就默认回到首页
    // 有bug--- 第一url参数看有没有获取到，  第二 看跳转过去的页面是什么页面，分tabbar和非tabar 
    wx.reLaunch({
      url: this.data.url || '/pages/index/index',
    })
  },

  // 下面是校验验证码的方法
  validateCode() {
    const pattern = /^\d{6}$/
    // 写这种校验的时候，先把所有失败的情况先写，然后return，最后在写正常的逻辑
    if (!this.data.code) {
      wx.utils.toast('请输入验证码')
      return false
    }
    if (!pattern.test(this.data.code)) {
      wx.utils.toast('验证码必须是6位数字')
      return false
    }

    // 这个函数里面 代码走到了这里，就证明所有的校验成功了，再返回true给外部函数使用
    return true
  },

  // 下面是校验手机的方法
  validateMobile(mobile) {
    const pattern = /^1[3456789]\d{9}$/
    if (!mobile) {
      wx.utils.toast('请输入手机号')
      return false
    }
    if (!pattern.test(mobile)) {
      wx.utils.toast('手机号格式错误，请求重新输入')
      return false
    }

    return true
  },

  // 点击获取验证码的函数定义
  async getCode() {
    // 点击验证码，将当前的手机号传递给后端，发请求，获取验证码
    // 1. 先来校验手机号  ---  前提是通过双向绑定的语法获取 页面上的mobile数据
    const result = this.validateMobile(this.data.mobile)
    if (!result) return
    // 对象前面的key都是字符串，之前都是加引号的，es6的简写可以不加， 只有键值对 冒号后面的才是 value，才是具体的值，才是变量
    const res = await wx.http.get(`/code?mobile=${this.data.mobile}`)
    // console.log(code1, code2); // res.data.data.code
    if (res.code !== 10000) return wx.utils.toast('获取验证码业务失败')

    // 下面代码是一个小技巧，让两个函数可以使用一个变量，声明一个更高作用域的变量
    myCode = res.data.code

    this.setData({
      countDownVisible: true
    })
  },

  clipTap() {
    // 下面代码类似于ctrl + c, 将一个数据 复制到剪贴板里面， 可以通过 ctrl + v 使用
    wx.setClipboardData({
      data: myCode,
    })
  },

  countDownChange(ev) {
    this.setData({
      timeData: ev.detail,
      // 倒计时每执行一次，就指向这个函数一次。当seconds等于小于0的时候，这个countDownVisible就是 false隐藏倒计时
      countDownVisible: ev.detail.minutes === 1 || ev.detail.seconds > 0,
    })
  },
})