Page({
  // 小程序是对象的写法（小程序写函数的时候不会生效也不会报错）， vue是函数返回一个对象
  data: {
    detail22: 222
  },
  // data() {
  //   // detail22: 222
  //   return {
  //     detail22: 222
  //   }
  // },
  // 不要在onLoad里面写太多的代码，他是入口，写太多代码，不利于后期维护
  onLoad(option) {
    console.log(option);
    this.getDetail(option.id) // 实参 --- 实际的参数 -- 函数调用的位置
  },
  // 函数定义的两种常见方式
  // function关键字 function fn(){}
  // 函数表达式  const fn = function () {}
  async getDetail(id) { // 形参 --- 形式上的，名称随便起的参数 ---  函数定义的位置 （）
    const res = await wx.http.get(`/announcement/${id}`)
    // console.log(res.data.data);
    // console.log(res.data);
    this.setData({
      detail: res.data
    })
  }
})