Page({
  goLogin() {
    wx.navigateTo({
      url: '/pages/login/index',
    })
  },
  data: {
    avatar: '',
    nickName: ''
  },
  onLoad() {
    // 发请求
    // 小程序第三天的时候 学过在 另外页面 通过 getCurrentPages 得到我的页面栈，手动掉onLoad
    // 还有一种，就是在onShow里面调用方法
    // 还有一种， 可以将整个数据 提升作用域，放到全局的appjs里面去，所有页面使用appjs里面的数据，从根本上就不用发请求了
    // this.getUserInfo()
  },
  onShow() {
    if (getApp().token) {
      this.getUserInfo()
    }
  },
  async getUserInfo() {
    const res = await wx.http.get('/userInfo')

    this.setData({
      avatar: res.data.avatar,
      nickName: res.data.nickName
    })
  },
})