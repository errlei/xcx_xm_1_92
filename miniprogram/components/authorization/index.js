Component({
  data: {
    isLogin: false
  },
  lifetimes: {
    // 生命周期函数选择attached 不用 created，因为里面不能访问this
    attached: function () {
      // 将一个任意类型的数据 转换为布尔值   两个感叹号
      this.setData({
        isLogin: !!getApp().token
      })

      const pages = getCurrentPages()
      console.log(pages[pages.length - 1]);
      const currentPage = pages[pages.length - 1]

      if (!this.data.isLogin) {
        wx.redirectTo({
          url: `/pages/login/index?aaaa=/${currentPage.route}`,
        })
      }
    },
  },
})