// map.js
Page({
  data: {
    latitude: 40.060539,
    longitude: 116.343847,
    detail: {},
    show: false,
    markers: [{
      id: 1,
      latitude: 40.060539,
      longitude: 116.343847,
      width: 20,
      height: 20
      // iconPath: '../../static/uploads/attachment.jpg',
    }, {
      id: 2,
      latitude: 40.060550,
      longitude: 116.343860,
      width: 50,
      height: 50,
      iconPath: '../../static/uploads/attachment.jpg',
    }]
  },
  // 下面写法是 对象解构的默认值， 如果不写，解构失败的试试，这个id就是 undefined
  onLoad({
    id
  }) {
    this.setData({
      id
    })
    this.getRepaireDetail(id)
  },
  async getRepaireDetail(id) {
    const res = await wx.http.get(`/repair/${id}`)
    this.setData({
      detail: res.data
    })
    console.log(this.data.detail);
  },
  test() {
    this.setData({
      show: true
    })
  },
  onClose() {
    this.setData({
      show: false
    })
    console.log('点击了取消按钮');
  },
  async submitCancel() {
    const res = await wx.http.put(`/cancel/repaire/${this.data.id}`)
    if (res.code !== 10000) return wx.utils.toast('取消报修请求失败')
    this.onClose()
    // 回退到上一个页面， 上几个页面，就要通过getCurrentPages来获取页码栈，在跳转
    wx.navigateBack()
  },
  updateInfo() {
    wx.navigateTo({
      // 分包的路由地址，在app。json里面 没有完整的路径，需要自己拼接
      // 过去以后， 新增和编辑公用同一个页面 具体是编辑还是新增  通过id来判断
      url: `/repair_pkg/pages/form/index?id=${this.data.id}`,
    })
  }
})