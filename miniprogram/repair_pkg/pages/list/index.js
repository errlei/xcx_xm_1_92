Page({
  data: {
    current: 1,
    pageSize: 10,
    repaireList: []
  },
  onLoad() {
    this.getRepaireList()
  },
  async getRepaireList(type) {
    const res = await wx.http.get(`/repair?current=${this.data.current}&pageSize=${this.data.pageSize}`)
    // setData 渲染的数据有大小限制，最大只能1MB， 尽可能减少这个数据的大小，就是将服务器返回的数据加工一下。wxml里面需要什么数据，就返回什么数据，不要的就不渲染
    const tmp = res.data?.rows.map(item => ({
      id: item.id,
      houseInfo: item.houseInfo,
      status: item.status,
      repairItemName: item.repairItemName,
      appointment: item.appointment,
      mobile: item.mobile
    }))
    if (type) {
      this.setData({
        total: res.data.total,
        repaireList: [...this.data.repaireList, ...tmp]
      })
    } else {
      this.setData({
        total: res.data.total,
        repaireList: tmp
      })
    }
  },
  goDetail(e) {
    console.log(e.currentTarget.dataset.id, e.mark.title);
    wx.navigateTo({
      url: `/repair_pkg/pages/detail/index?id=${e.currentTarget.dataset.id}`,
    })
  },
  addRepair() {
    // git 代码版本管理工具
    // 解构赋值： 批量给变量赋值
    // const {
    //   a,
    //   b: b1 = 3,
    //   c: {
    //     c: c2
    //   }
    // } = {
    //   a: 1,
    //   b1: 2,
    //   c: {
    //     c: 4
    //   }
    // }
    // // 1. 几个变量？ a  b1 c2
    // console.log(a, b1, c2);
    // wx.navigateTo({
    //   url: '/repair_pkg/pages/form/index',
    // })
  },
  onPullDownRefresh() {
    console.log('页面级别的下拉');
    // this.data.current = 1
    // console.log(this.data.current);
    // this.getRepaireList()
    wx.stopPullDownRefresh()
  },
  fn() {
    console.log('scroll view 一个小区域的 下拉');
    this.data.current = 1
    // this.data.repaireList = []
    this.getRepaireList()
  },

  // 页面和区域的上拉-- 加载下一页  page++
  onReachBottom() {
    // 我们这个 报修页面，写点css 导致了我们这个页面不会出现滚动条  page{ heieht: 100vh} 没有滚动条就没有触底的说法
    console.log('这个函数是页面 上拉触底了执行的函数，');
  },
  // 我们这个页面需要使用的是 scroll-view的触底事件
  fn2() {
    console.log('scroll-view的触底， 这里就page++  发请求');

    // 当前页码值 * 每页条数  和 total对比
    //  2  * 10 <  13  如果比13总数小，就接着发请求，如果比他大，就不发请求了
    if (this.data.current * this.data.pageSize < this.data.total) {
      this.data.current++
      // 这里传递参数，如果有，就证明是 上拉的请求，需要拼接。如果这个请求没有参数，就是普通的赋值
      // 参数1是随便写的
      this.getRepaireList(1)
    }
  }
})