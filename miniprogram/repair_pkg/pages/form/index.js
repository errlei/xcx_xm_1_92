import dayjs from 'dayjs'

Page({
  data: {
    houseName: {
      name: '请选择报修房屋'
    },
    repairName: {
      name: '请选择维修项目'
    },
    mobile: '',
    appointment: '',
    description: '',
    attachment: [],

    currentDate: new Date('2025-9-20').getTime(),
    houseLayerVisible: false,
    repairLayerVisible: false,
    dateLayerVisible: false,
    houseList: [],
    repairItem: [],
  },
  onLoad({
    id
  }) {
    if (id) {
      // 编辑
      wx.setNavigationBarTitle({
        title: '修改报修信息',
      })
      this.setData({
        id
      })
      this.getHouseDetail()
    }

    // 下面两个函数调用，在新增和编辑的时候都要调用
    this.getHouseList()
    this.getRepairItem()
  },
  // 获取详情
  async getHouseDetail() {
    const res = await wx.http.get(`/repair/${this.data.id}`)
    console.log(12211, res);

    this.setData({
      houseName: {
        // 为什么这里 报修房屋要对象里面写两个字段？
        // 因为 页面上展示给用户看的是 汉语汉字。 后端服务器 接口文档 需要的是id
        id: res.data.houseId,
        name: res.data.houseInfo,
      },
      repairName: {
        id: res.data.repairItemId,
        name: res.data.repairItemName,
      },
      mobile: res.data.mobile,
      appointment: res.data.appointment,
      description: res.data.description,
      attachment: res.data.attachment,
    })
  },
  // 下面两个函数是 针对 报修房屋
  async getHouseList() {
    // /house接口如果没有数据，就换成/room接口也可以， 只不过返回的值里面 name是用户名，不是小区名称要，改一下
    const res = await wx.http.get('/room')
    const tmp = res.data.map(item => ({
      id: item.id,
      name: `${item.point}${item.building}${item.room}`
    }))
    console.log(111, tmp);

    this.setData({
      houseList: tmp
    })
  },
  onHouseSelect(e) {
    this.setData({
      houseName: e.detail
    })
  },
  // 下面两个函数是针对维修项目, 一个是获取列表，一个是选中某一项以后
  async getRepairItem() {
    const res = await wx.http.get('/repairItem')

    this.setData({
      repairItem: res.data
    })
  },
  onRepaireSelect(e) {
    this.setData({
      repairName: e.detail
    })
  },
  // 下面的函数是关于时间选择的
  datetimeConfirm(e) {
    console.log('11111', e.detail); // 时间戳
    // currentDate 这个变量是 打开时间控件，默认的值
    // appointment 这个变量是后端需要的参数
    this.setData({
      appointment: dayjs(e.detail).format('YYYY-MM-DD')
    })
    // 关于时间格式的修改： 先看组件自己有没有格式化的方法，如果没有在用 dayjs
    this.closeDateLayer()
  },
  datetimeCancel() {
    this.closeDateLayer()
  },
  // 下面函数是上传图片的 ---- 当写代码写到一半不知道怎么写了？ 就去看需求，看我们要干嘛，然后在看是不是需要看对应的官网文档，找demo代码来写。最后在结合我们同事写的 上传代码，直接拿过来用
  afterRead(event) {
    const {
      file
    } = event.detail;
    // 当设置 mutiple 为 true 时, file 为数组格式，否则为对象格式
    wx.uploadFile({
      url: 'https://live-api.itheima.net/upload', // 仅为示例，非真实的接口地址
      filePath: file.url,
      name: 'file',
      header: {
        Authorization: getApp().token
      },
      success: (res) => {
        const data = JSON.parse(res.data)
        // const fileList1 = this.data.fileList || [];
        // [{id:1, url: 'aaa'}, {id:1, url: 'aaa'} ]
        // fileList.push({ ...file, url: res.data });
        // fileList.push({
        //   ...data.data
        // });

        // 现在这种写法也可以，是高级写法，从对象解构里面 添加默认值，类似于87行的代码
        // const {
        //   attachment = []
        // } = this.data;
        // attachment.push(data.data);

        // 下面代码稍微简单一些，就是先把js里面的数组修改追加元素，然后在通过setData更新页面
        this.data.attachment.push(data.data);
        this.setData({
          attachment: this.data.attachment
        });
      },
    });
  },




  openHouseLayer() {
    this.setData({
      houseLayerVisible: true
    })
  },
  closeHouseLayer() {
    this.setData({
      houseLayerVisible: false
    })
  },
  openRepairLayer() {
    this.setData({
      repairLayerVisible: true
    })
  },
  closeRepairLayer() {
    this.setData({
      repairLayerVisible: false,
    })
  },

  openDateLayer() {
    this.setData({
      dateLayerVisible: true
    })
  },
  closeDateLayer() {
    this.setData({
      dateLayerVisible: false
    })
  },
  async goList() {
    // const {
    //   fileList: fileList = []
    // } = {
    //   name: '张三',
    //   fileList1: [1, 2, 3, 4, 4]
    // };
    // console.log(fileList);
    // console.log(this.data);

    if (!this.data.houseName.id) return wx.utils.toast('请选择房屋信息！')
    if (!this.data.repairName.id) return wx.utils.toast('请选择维修项目！')
    if (!/^1[3-9]\d{9}$/.test(this.data.mobile)) return wx.utils.toast('请填写正确手机号！')
    if (!this.data.appointment) return wx.utils.toast('请选择日期！')
    if (!this.data.description) return wx.utils.toast('请填写问题描述！')
    if (!(Array.isArray(this.data.attachment) && this.data.attachment?.length > 0)) return wx.utils.toast('请上传问题附件')

    const obj = {
      houseId: this.data.houseName.id,
      repairItemId: this.data.repairName.id,
      mobile: this.data.mobile,
      appointment: this.data.appointment,
      description: this.data.description,
      attachment: this.data.attachment
    }

    if (this.data.id) {
      obj.id = this.data.id
    }

    const res = await wx.http.post('/repair', obj)
    if (res.code !== 10000) return wx.utils.toast('添加维修信息失败')
    // 你在工作里面 使用的编程式多还是声明式导航多一些？ -- 小程序，vue都是一样说的
    // 这个要看情况，一般如果是模板列表里面跳转页面，那么采用声明式导航，因为模板上面就可以直接使用参数，不需要传参到js里面。 还有些情况，如果是发送请求成以后，才能跳转，那么必须使用 编程式导航
    wx.reLaunch({
      url: '/repair_pkg/pages/list/index',
    })
  },
})