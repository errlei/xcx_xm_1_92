let currentId = 0
Page({
  data: {
    dialogVisible: false,
    houseList: []
  },

  onLoad() {
    this.getHouseList()
  },

  async getHouseList() {
    const res = await wx.http.get('/room')
    // 发完请求，先做错误的处理， 必须写return  控制代码的流程
    if (res.code !== 10000) return wx.utils.toast(res.message || '获取房屋列表请求失败')

    console.log(res.data);
    this.setData({
      houseList: res.data
    })
  },

  swipeClose(ev) {
    console.log(11111, ev.target.dataset.id);
    currentId = ev.target.dataset.id
    const {
      position,
      instance
    } = ev.detail

    if (position === 'right') {
      // 显示 Dialog 对话框
      this.setData({
        dialogVisible: true,
      })

      // swiper-cell 滑块关闭
      instance.close()
    }
  },

  async dialogClose(e) {
    if (e.detail === 'confirm') {
      await wx.http.delete(`/room/${currentId}`)
      this.getHouseList()
    }
  },

  goDetail(e) {
    // 这里自己分析以下 为什么 target没有id  currentTarget有id。 分析思路就是点击的是哪个元素 +  绑定这个事件的又是哪个元素？
    wx.navigateTo({
      url: `/house_pkg/pages/detail/index?id=${e.currentTarget.dataset.id}`,
    })
  },

  addHouse() {
    wx.navigateTo({
      url: '/house_pkg/pages/locate/index',
    })
  },
})