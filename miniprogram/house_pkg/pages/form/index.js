Page({
  data: {
    // 经验： 一般的添加是绝对没有id的，只有编辑才必须要id
    // 后面证明这个接口是 新增和编辑公用的
    // "id": "",
    "point": "",
    "building": "",
    "room": "",
    "name": "",
    "gender": 0,
    "mobile": "",
    idcardFrontUrl: '',
    idcardBackUrl: '',
  },
  onLoad(options) {
    // 点击提交审核，发起添加房屋的请求， 思维步骤
    // wx.post(xxx)  --->  url ---> 接口文档  ---> 要10个参数
    // 一个个去准备 后端接口 需要的参数
    // id  point  room
    // 现在就要在 data上面 准备好所有的字段， 在一个个去填写数据，准备数据， 然后才能看请求怎么发送
    console.log(options);
    if (options.id) {
      wx.setNavigationBarTitle({
        title: '编辑房屋信息'
      })
      this.getDetail(options.id)
    } else {
      this.setData({
        point: options.point,
        building: options.building,
        room: options.room
      })
    }
  },
  async getDetail(id) {
    const res = await wx.http.get(`/room/${id}`)

    this.setData({
      // name: 'zs',
      // mobile: 122222,
      ...res.data
    })
  },
  async goList() {
    // 做校验，先把错误的判断一写，直接return
    if (!this.data.name) return wx.utils.toast('姓名必填')
    if (!/^1[3-9]\d{9}$/.test(this.data.mobile)) return wx.utils.toast('手机号格式错误')
    if (!this.data.idcardFrontUrl || !this.data.idcardBackUrl) return wx.utils.toast('身份证照片必传')

    delete this.data.__webviewId__
    if (this.data.id) {
      delete this.data.status
    }

    const res = await wx.http.post('/room', this.data)

    // 首先发请求，不管前端还是后端都要校验，前端不写不要紧，后端是一定要写的。 因为前端可以通过一个非法手段
    // 绕过我们写的前端校验规则。 有些人可以通过 postman 直接发请求，不通过小程序按钮点击。 还有有些在路由器，或者网络层面 做一些参数跳过的手段，我们前端也监听不到。 所以前端做校验只能说 减轻一点服务器的压力，真正的有用的校验在后端

    if (res.code !== 10000) return wx.utils.toast(res.message || '添加房屋信息失败')

    wx.reLaunch({
      url: '/house_pkg/pages/list/index',
    })
  },
  removePicture(ev) {
    // 移除图片的类型（身份证正面或反面）
    const type = ev.mark?.type
    this.setData({
      [type]: ''
    })
  },
  choosePicture(e) {
    const that = this
    console.log(e.mark?.type);
    const type = e.mark?.type
    // 以前的旧的方法 chooseImage  2.25 以后就停止维护
    wx.chooseMedia({
      count: 9,
      mediaType: ['image', 'video'],
      sourceType: ['album', 'camera'],
      maxDuration: 30,
      camera: 'back',
      success(res) {
        // res里面的图片地址，就是等下上传到服务器的 图片临时路径
        // 不管web还是小程序， 选择了本地照片以后，都应该将照片在上传到服务器里面，永久保存，不然照片一刷新就没有啦
        console.log(res.tempFiles)
        wx.uploadFile({
          url: 'https://live-api.itheima.net/upload',
          filePath: res.tempFiles[0].tempFilePath,
          name: 'file',
          // 下面这个属性开始不知道，是请求报了401的 toekn问题，我们才发现 需要和我们内置的http请求保持一致
          // 给这个上传的请求手动在header里面添加token --- 并且现在要看 微信官方文档 和 我们后端的文档-- 两个都要比对好才可以， 缺少什么参数，怎么传递就要一个个属性试出来，靠经验的积累
          header: {
            Authorization: getApp().token
          },
          success(res1) {
            const data = JSON.parse(res1.data)
            console.log(111, data);
            // if (e.mark?.type === 'idcardFrontUrl') {
            //   that.setData({
            //     idcardFrontUrl: res1.xxx
            //   })
            // } else if (e.mark?.type === 'idcardBackUrl') {
            //   that.setData({
            //     idcardBackUrl: res1.xxx
            //   })
            // }
            // 百度： 小程序 setData 动态属性 设置
            that.setData({
              [type]: data.data?.url
            })
          }
        })
      }
    })
  }
})