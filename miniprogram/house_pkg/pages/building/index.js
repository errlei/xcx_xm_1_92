Page({
  data: {
    title: '',
    buildings: []
  },
  onLoad({
    title
  }) {
    this.setData({
      title
    })
    this.getBuildings(title)
  },
  getBuildings(title) {
    // 随机生成 3 - 10
    // Math.floor()  向下取整  Math.ceil() 向上取整   Math.四舍五入 
    // Math.random()  [0, 1)  ---->  * 10  ---> [1, 10)
    // Math.floor(Math.random() * 10) ---> [0, 10)  + 3 --->  [3, 13)
    // Math.floor(Math.random() * 8) ---> [0, 8)  + 3 --->  [3, 11)

    // 获取任意的min -- max的随机整数  3 - 10
    // Math.floor(Math.random() * (max - min  + 1)) + min
    const sizes = Math.floor(Math.random() * 8) + 3
    const buildings = []
    for (let i = 1; i <= sizes; i++) {
      const str = title + i + '号楼'
      buildings.push(str)
    }

    this.setData({
      buildings: buildings
    })
  }
})