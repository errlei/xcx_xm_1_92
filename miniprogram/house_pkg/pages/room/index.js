Page({
  data: {
    // point当前社区的信息， building 当前是几号楼
    point: '',
    building: '',
    rooms: []
  },
  // onLoad({point, building}) {
  onLoad(options) {
    console.log(options);
    const index = options.building
    // 字符串的获取api
    // 1. 获取索引号  indexOf   str.split('').findIndex(item => item === '号')
    // 2. str.charAt(7)  str.slice(7,8)  str.substr(7,1)  str.substring(7, 8)
    this.setData({
      point: options.point,
      building: options.building.charAt(options.building.indexOf('号楼') - 1)
    })

    this.createdRooms()
  },
  // 下面这个函数是 来构建当前页面有多少个房间 假数据的 函数
  createdRooms() {
    // 101  1 楼层-- 1- 33层。  最后一个1 是每一层的房间数目  一般一层是4个房间
    // 当前选择房间页面 有多少个 房间，这个数字是随机构建的
    // roomSize  每页随机有 5-10个房间数量， rooms 每个房间组成的数组
    const rooms = []
    const roomSize = Math.floor(Math.random() * 6) + 5

    // 武湖新天地2号楼 2303
    for (let i = 0; i < roomSize; i++) {
      let str = ''
      // str += this.data.point
      // str += this.data.building + '号楼'
      // floor 当前多少层   room 当前楼层的房间号 一般一层有个房间
      let floor = Math.floor(Math.random() * 33) + 1
      let room = Math.floor(Math.random() * 4) + 1
      // str += floor + 0 + room 

      //在公司里面问同事： 我们是问一些 变量和方法的 使用，是什么意思？有什么注意点？
      // 不能问的是一些基本的语法， 比如 小程序里面 循环怎么写？

      str = `${this.data.point}${this.data.building}号楼 ${floor}0${room}`
      if (rooms.includes(str)) continue

      rooms.push(str)
    }

    this.setData({
      rooms
    })
  },

  goToFrom(e) {
    // 变量初始化=== 变量的声明 + 变量的复制
    const tmp = e.target.dataset.item.split(' ')[1]
    wx.navigateTo({
      url: `/house_pkg/pages/form/index?point=${this.data.point}&building=${this.data.building}&room=${tmp}`,
    })
  }

})