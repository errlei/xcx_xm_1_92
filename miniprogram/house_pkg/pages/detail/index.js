Page({
  data: {
    houseDetail: {}
  },
  onLoad({
    id
  }) {
    this.setData({
      id
    })
    this.getDetail(id)
  },
  async getDetail(id) {
    const res = await wx.http.get(`/room/${id}`)

    this.setData({
      houseDetail: res.data
    })
  },
  editHouse() {
    wx.navigateTo({
      url: `/house_pkg/pages/form/index?id=${this.data.id}`,
    })
  },
})