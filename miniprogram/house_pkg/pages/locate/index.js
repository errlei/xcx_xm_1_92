import qqMapObj from '../../../utils/qqmap'

Page({
  data: {
    address: '',
    list: []
  },
  onLoad() {
    this.getLocal()
  },
  chooseLocal() {
    // chooseLocation 这个api支持promise的写法，
    // 在写setData的时候，既可以使用async + await的写法，又可以使用then +catch的写法
    // const res = await wx.chooseLocation()
    // console.log(res);
    // this.setData({
    //   address: res.address
    // })
    wx.chooseLocation().then(res => {
      this.setData({
        address: res.address
      })

      this.searchKeyword(res.latitude, res.longitude)
    })

  },
  async getLocal() {
    const res = await wx.getLocation()
    this.transformLaLoToWord(res.latitude, res.longitude)
    this.searchKeyword(res.latitude, res.longitude)
  },

  // 下面两个函数是qqmap的工具函数，只是经纬度参数不一样，抽离封装为函数以后，设置两个形参即可
  transformLaLoToWord(latitude, longitude) {
    // 下面的代码是新增的， 因为获取定位的数据个人开发者调用每天有上限
    // 我们只需要第一次调用完以后，就将这个数据 缓存起来，后面冲缓存里面拿，不需要从腾讯位置服务里面拿就好
    const res1 = wx.getStorageSync('hm_address')

    if (!res1) {
      qqMapObj.reverseGeocoder({
        // location: {
        //   latitude: res.latitude,
        //   longitude: res.longitude
        // },
        location: [latitude, longitude].join(','),
        // this的指向你说一说？ 把前面背的八股文巴拉输出一遍， 再说 我之前项目里面，写过很多小程序的腾讯储存桶，腾讯位置定位服务，里面很多api的异步接口都是 success这种回调函数的写法，我们在掉this.setData的时候，会出现值绑定不上去的问题，这种情况一般是this指向混乱。我一般有两种修改问题的方式： 1. 在外部定义一个that变量，保存this 2.将success回调函数改写为es6的 箭头函数。
        success: (res) => {
          this.setData({
            address: res.result.address
          })
          wx.setStorage({
            key: 'hm_address',
            data: res.result.address
          })
        }
      })

      return
    }

    this.setData({
      address: res1
    })
  },

  searchKeyword(latitude, longitude) {
    // 我们只需要第一次调用完以后，就将这个数据缓存起来，后面冲缓存里面拿，不需要从腾讯位置服务里面拿就好
    const res1 = wx.getStorageSync('hm_shequ')

    if (!res1) {
      const that = this
      qqMapObj.search({
        keyword: '社区',
        location: {
          latitude: latitude,
          longitude: longitude
        },
        success: (res) => {
          // 加工数组
          const tmpArr = res.data.map(item => ({
            id: item.id,
            title: item.title
          }))
          // setData单次设置的数据最大只能是1MB， 所以将服务器返回的数据 加工一下。 去掉很多页面渲染不需要的字段
          that.setData({
            list: tmpArr
          })
          // 为了防止接口调用次数过多的问题，我们将 数据持久化以下
          wx.setStorage({
            key: 'hm_shequ',
            data: tmpArr
          })
        }
      })
      return
    }

    this.setData({
      list: res1
    })
  }
})