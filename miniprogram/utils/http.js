import http from 'wechat-http'

// 在我们的普通js文件里面， 如果直接getApp()，获取的是 undeifned
// const app = getApp()

// 请求基础地址
http.baseURL = "https://live-api.itheima.net"

// 请求拦截器
http.intercept.request = function (options) {
  const header = {}
  const app = getApp()
  if (app.token) header.Authorization = app.token

  // 下面两个方法都可以， 之前的浅拷贝也用过  Obejct.assign  与 展开运算符
  // options.header = Object.assign({}, header, options.header)
  options.header = {
    ...header,
    ...options.header
  }

  return options
}
// 响应拦截器
http.intercept.response = (res) => {
  // console.log(res.config?.url?.includes('refreshToken'));
  if (res.data?.code === 401) {
    // 如果url里面有refreshToken 这个字符串，正常要重复的去调用刷新token请求了，里面return掉，就不会死循环
    if (res.config?.url?.includes('refreshToken')) {
      wx.redirectTo({
        url: '/pages/login/index',
      })
      return
    }
    // 只要进入了这个if分支，就证明token失效了
    // 以前我们是直接返回登录页面。 现在换一种方式， 使用刷新refreshToken 发起一个更新token 的请求
    const app = getApp()
    // 下面是发起刷新token的请求，重新获取新的token
    http({
      url: '/refreshToken',
      method: 'post',
      header: {
        Authorization: app.refreshToken
      }
    }).then(res2 => {
      app.setToken(res2.data.token, res2.data.refreshToken)

      // 这里是then catch的写法， 不是课件的async await， 所以 刷新token请求成功以后，把之前请求失败的请求
      // 在发送一遍，这个过程就叫无感刷新

      // http({...res.config, ...{
      //   header: {
      //     Authorization: app.token
      //   }
      // }})
      // 无感刷新
      http(Object.assign(res.config, {
        header: {
          Authorization: app.token
        }
      }))
    })
  }
  // 给所有请求脱壳处理， （在页面那里面 不需要在用 res.data.data）
  // return res
  return res.data
}

wx.http = http

export default http