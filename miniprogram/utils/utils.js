const utils = {
  // https://es6.ruanyifeng.com/
  // 函数的默认参数
  toast(title = "默认参数") {
    // 旧写法 （或者说 babel将高版本的js转换为低版本的js，这个默认参数就是通过 逻辑或的方式来写的）
    // title = title || '默认参数'  前面title为假 就往后走
    wx.showToast({
      title,
      icon: 'none'
    })
  }
}
// JS的四种模块化规范
// import + export
// require + exports = module.exports
// es module  ---  commonJS --AMD  CMD
export default utils

// 另一种不需要导入就可以使用的方法
wx.utils = utils