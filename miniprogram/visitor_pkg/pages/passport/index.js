import drawQrcode from '../../../utils/weapp.qrcode.esm'

// https://github.com/yingye/weapp-qrcode
// weapp-qrcode  小程序生成二维码的包

drawQrcode({
  width: 200,
  height: 200,
  canvasId: 'myQrcode',
  // ctx: wx.createCanvasContext('myQrcode'),
  text: '啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊',
  // v1.0.0+版本支持在二维码上绘制图片
  image: {
    // imageResource: '../../images/icon.png',
    dx: 70,
    dy: 70,
    dWidth: 60,
    dHeight: 60
  }
})

Page({
  data: {
    // timeData: 6 * 1000
    flag: true
  },
  onLoad({
    id
  }) {
    console.log('进入到二维码页面', id);
    this.getDetail(id)
  },
  async getDetail(id) {
    const res = await wx.http.get(`/visitor/${id}`)
    console.log(11, res);
    // this.setData({
    //   time: res.data.validTime
    // })
    // this.setData(res.data)
    this.setData({
      ...res.data,
      ...{
        validTime: 6
      }
    })
  },
  onChange(e) {
    this.setData({
      timeData: e.detail,
    });
  },
  timeFinist() {
    this.setData({
      flag: false
    })
  },
  // 下面这个函数是 让右上角可以点击分享
  onShareAppMessage() {
    return {
      title: '我随便写',
      path: '/visitor_pkg/pages/passport/index',
      imageUrl: 'https://enjoy-plus.oss-cn-beijing.aliyuncs.com/images/share_poster.png',
    }
  },
  // 让用户点击右上角分享到朋友圈
  onShareTimeline() {
    return {
      title: 'xxxxxxxx'
    }
  },
  saveToPhoto() {
    // 可以是临时文件路径或永久文件路径 (本地路径)
    // wx.saveImageToPhotosAlbum({
    //   filePath: '../../images/1.jpg',
    //   success(res) {
    //     console.log(111, res);
    //   }
    // })
    // 下面这个api就是获取图片的 临时地址
    // wx.getImageInfo({
    //   src: this.data.url,
    //   success: (res) => {
    //     console.log(111, res);
    //     wx.saveImageToPhotosAlbum({
    //       filePath: res.path, // 临时路径， 格式就是 http://tmp/xxxxxx
    //       success: (res2) => {
    //         console.log(222, res2);
    //       }
    //     })
    //   }
    // })
    wx.downloadFile({
      url: this.data.url,
      success(res) {
        console.log(11, res);
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success(res2) {
            console.log(33, res2);
          }
        })
      }
    })
  }
})