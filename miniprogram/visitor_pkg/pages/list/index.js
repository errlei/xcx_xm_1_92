Page({
  data: {
    list: []
  },
  onLoad() {
    this.getList()
  },
  async getList() {
    const res = await wx.http.get(`/visitor?current=1&pageSize=20`)

    this.setData({
      list: res.data.rows
    })
    // this.list = res.data.rows  vue写法
  },
  goPassport(e) {
    console.log(e);
    wx.navigateTo({
      url: `/visitor_pkg/pages/passport/index?id=${e.currentTarget.dataset?.id}`,
    })
  },
})