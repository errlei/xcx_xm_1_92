import dayjs from 'dayjs'
Page({
  data: {
    "houseId": "",
    "name": "",
    "gender": 0,
    "mobile": "",
    "visitDate": "",
    currentDate: 1721318400000,
    dateLayerVisible: false,
    houseLayerVisible: false,
    houseList: [],
  },
  onLoad() {
    this.getHouseList()
  },
  // 下面两个代码是 参数 同事A的代码，获取房屋信息
  async getHouseList() {
    // /house接口如果没有数据，就换成/room接口也可以， 只不过返回的值里面 name是用户名，不是小区名称要，改一下
    const res = await wx.http.get('/room')
    const tmp = res.data.map(item => ({
      id: item.id,
      name: `${item.point}${item.building}${item.room}`
    }))
    console.log(111, tmp);

    this.setData({
      houseList: tmp
    })
  },
  onHouseSelect(e) {
    console.log(1113, e.detail);
    this.setData({
      houseName: e.detail
    })
    console.log(33, this.data);
  },
  // 下面两个函数是 借鉴同事B的， 日期插件的代码
  // 下面的函数是关于时间选择的
  datetimeConfirm(e) {
    console.log('11111', e.detail); // 时间戳
    // currentDate 这个变量是 打开时间控件，默认的值
    // appointment 这个变量是后端需要的参数
    this.setData({
      visitDate: dayjs(e.detail).format('YYYY-MM-DD')
    })
    // 关于时间格式的修改： 先看组件自己有没有格式化的方法，如果没有在用 dayjs
    this.closeDateLayer()
  },
  datetimeCancel() {
    this.closeDateLayer()
  },

  openHouseLayer() {
    this.setData({
      houseLayerVisible: true
    })
  },
  closeHouseLayer() {
    this.setData({
      houseLayerVisible: false
    })
  },
  openDateLayer() {
    this.setData({
      dateLayerVisible: true
    })
  },
  closeDateLayer() {
    this.setData({
      dateLayerVisible: false
    })
  },
  onChange(event) {
    this.setData({
      gender: event.detail,
    });
  },
  async goPassport() {
    // console.log(this.data);
    // 发请求成功以后， 在跳转页面
    const obj = {
      "houseId": this.data.houseName?.id,
      "name": this.data.name,
      "gender": this.data.gender,
      "mobile": this.data.mobile,
      "visitDate": this.data.visitDate,
    }

    const res = await wx.http.post('/visitor', obj)
    if (res.code !== 10000) return wx.utils.toast('访客信息接口添加失败')
    wx.reLaunch({
      url: '/visitor_pkg/pages/passport/index',
    })
  },
})