#### 核心-- 计算导航栏高度 -- 胶囊按钮上下左右间距一样

![导航栏等高度](./导航栏等高度.jpg)



![导航栏高度示例图](./导航栏高度示例图.jpg)

**自定义导航栏**

1.**状态栏**（时间和电量显示那一栏）
**+**
2.状态栏和标题栏之间的**间距**
**+**
3.**标题栏**（小程序胶囊按钮那一栏）
**+**
4.标题栏和正文区域之间的**间距**



```
/**
 * 获取微信小程序菜单栏(胶囊)信息
 * 菜单按键宽度：width
 * 菜单按键高度：height
 * 菜单按键上边界坐标：top
 * 菜单按键右边界坐标：right
 * 菜单按键下边界坐标：bottom
 * 菜单按键左边界坐标：left
 */
wx.getMenuButtonBoundingClientRect();

> 重点: 此api返回的是胶囊按钮在页面中的的上下左右坐标的绝对位置
> 注意：在模拟器使用时记得把视图百分比调为100%，否则可能会导致获取数据不准确
```

```
// 获取系统信息
    const systemInfo = wx.getSystemInfoSync();
    // 胶囊按钮位置信息
    const menuButtonInfo = wx.getMenuButtonBoundingClientRect();
    // 导航栏高度 = 状态栏到胶囊的间距（胶囊上坐标位置-状态栏高度） * 2 + 胶囊高度 + 状态栏高度
    this.globalData.navBarHeight = (menuButtonInfo.top - systemInfo.statusBarHeight) * 2 + menuButtonInfo.height + systemInfo.statusBarHeight;
    // 状态栏和菜单按钮(标题栏)之间的间距
    // 等同于菜单按钮(标题栏)到正文之间的间距（胶囊上坐标位置-状态栏高度）
    this.globalData.menuBottom = menuButtonInfo.top - systemInfo.statusBarHeight;
    // 菜单按钮栏(标题栏)的高度
    this.globalData.menuHeight = menuButtonInfo.height;
```



app.js

```
onLaunch() {
  this.getToken()

  // 获取系统信息
  const systemInfo = wx.getSystemInfoSync();
  // 胶囊按钮位置信息
  const menuButtonInfo = wx.getMenuButtonBoundingClientRect();
  console.log(333, systemInfo, menuButtonInfo);
  // 导航栏高度 = 状态栏到胶囊的间距（胶囊上坐标位置-状态栏高度） * 2 + 胶囊高度 + 状态栏高度
  this.globalData.navBarHeight = (menuButtonInfo.top - systemInfo.statusBarHeight) * 2 + menuButtonInfo.height + systemInfo.statusBarHeight;
  // 状态栏和菜单按钮(标题栏)之间的间距
  // 等同于菜单按钮(标题栏)到正文之间的间距（胶囊上坐标位置-状态栏高度）
  this.globalData.menuBottom = menuButtonInfo.top - systemInfo.statusBarHeight;
  // 菜单按钮栏(标题栏)的高度
  this.globalData.menuHeight = menuButtonInfo.height;
  this.globalData.top = menuButtonInfo.top;
  this.globalData.leftWidth = systemInfo.screenWidth - menuButtonInfo.width - (systemInfo.screenWidth - menuButtonInfo.right) * 2;
},
// 数据都是根据当前机型进行计算，这样的方式兼容大部分机器
globalData: {
  top: 0,
  leftWidth: 0,
  //全局数据管理
  navBarHeight: 0, // 导航栏高度
  menuBottom: 0, // 胶囊距底部间距（顶部间距也是这个）
  menuHeight: 0, // 胶囊高度
},
```



## ios安全区域 ---  微信小程序和 h5 网页需要针对这种情况进行适配

安全区域：刘海， 圆角，底部小黑条 等

一些名词： 从iphoneX开始，物理Home键，全面屏，非全面屏

普遍问题就是底部按钮或者选项卡与底部黑线重叠





![安全区域](./安全区域.jpg)

**适配方案：**当前有效的解决方式有几种

1. 使用已知底部小黑条高度34px/68rpx来适配

   1. 这种方式是根据实践得出，通过物理方式测出iPhone底部的小黑条高度是34px。直接根据该值，设置margin-bottom、padding-bottom、height也能实现。 但有个前提，需要判断当前机型是需要适配安全区域的机型

2. 使用苹果官方推出的css函数env()、constant()适配

   ```
   这种方案是苹果官方推荐使用env()，constant()来适配，开发者不需要管数值具体是多少
   env和constant是IOS11新增特性，有4个预定义变量
   safe-area-inset-  TOP/LEFT/RIGHT/BOTTOM
   
   constant和env不能调换位置
   
   padding-bottom: constant(safe-area-inset-bottom); /*兼容 IOS<11.2*/
   padding-bottom: env(safe-area-inset-bottom); /*兼容 IOS>11.2*/
   ```

   

3. 使用微信官方API，getSystemInfo()中的safeArea对象进行适配

   1. 参考上面自定义导航栏的高度获取方式
   2. 安全区域高度 = screenHeight 屏幕高度 - height 可视区域高度 - statusBarHeight 状态栏高度





## H5上适配安全区域

采用viewport+env+constant方案

```
将viewport设置为cover，env和constant才能生效
<meta name="viewport" content="width=device-width,initial-scale=1.0,viewport-fit=cover">
```

```
同时设置env和constant代码，同样env()和constant()需要同时存在，而且顺序不能换

/* 可以通过增加padding-bottom来适配 */
padding-bottom: constant(safe-area-inset-bottom); /*兼容 IOS<11.2*/
padding-bottom: env(safe-area-inset-bottom); /*兼容 IOS>11.2*/

/* 可以通过margin-bottom来适配 */
margin-bottom: constant(safe-area-inset-bottom);
margin-bottom: env(safe-area-inset-bottom);

/* 或者改变高度*/
height: calc(55px +  constant(safe-area-inset-bottom));
height: calc(55px +  env(safe-area-inset-bottom));
```













找工作 面试题 八股文 一定要非常熟悉 ---  让你们去面试

人资 14天  小程序8天 + uniapp + vue3  12天  ----   进入公司能正常的写代码



快乐修  2023.1 --- 2023.4 

项目的技术： 原生小程序 + vant-ui +sass +腾讯云储存 + 腾讯位置服务

项目的介绍：快乐修这个小程序主要是服务于周边社区的小程序。主要为附件社区的具名 提供生活家电报修，访客门禁，代送外卖等服务器。该项目主要包含有 房屋管理，报修管理，访客管理，外卖管理 等等。

负责的模块：房屋分包里面所有的模块，涉及到列表，详情，地图等常用功能。报修管理等功能

主要职责：

1. 封装过小程序里面的请求模拟，代替原生的wx.request。使用一个第三方包 wechat-http，实现了有promise，请求响应拦截器
2. 封装了authorization登录检测组件，代替了vue-router里面的路由守卫。核心就是动态显示隐藏插槽的功能
3. 在访问页面页面数据的时候，使用getCuurentPage 页面栈，完成组件之间通信的功能
4. 还用过小程序的一些setClipboardData剪切板，
5. 使用过小程序的地理定位getLocation 结合腾讯位置服务。实现地图业务的一些功能。使用过里面常见的api有，逆地址解析reverseGeocoder等等
6. （自己心里记住的话，开发小程序比较简单，大部分的业务都是发请求，然后通过setData渲染数据到wxml里面去。我觉得比较难的点就是一些特殊的ui效果会麻烦一点， 之前在公司里面都是一个人完成的小程序。感觉没有什么压力。 小程序原生的目录结构不好用，我自己搭建小程序的时候，吧目录结构会优化一般，扒拉巴拉。 在给另外一个同事写几个页面，就快速完成）
7. 还做过一个小程序自定义的导航栏，核心就是涉及到 胶囊按钮的宽高距离周边的距离，和状态栏的高度























